import * as d3 from 'd3';
import moment from 'moment';

const dennyd=[];

d3.csv('./dataSet/denny.csv', function(d){
  const data=[d];
  for(let i=0; i< data.length; i++){

        let startTime = moment.utc(`${data[i].startTime} ${data[i].startap}`,"HH:mm:ss a");
        let endTime = moment.utc(`${data[i].stopTime} ${data[i].stopap}`,"HH:mm:ss a");
        let duration = moment.duration(endTime.diff(startTime));
        let hours = parseInt(duration.asHours());
        if(hours>0)
          dennyd.push(hours);
        if(hours<0){
          hours= hours* (-1);
          dennyd.push(hours);
        }

    }
});

console.log(dennyd);


export default function() {
  return [
    { name: "Danny", data: dennyd },
    { name: "Harry" },
    { name: "Ho" },
    { name: "Kay" },
    { name: "Love" }
  ];
}

