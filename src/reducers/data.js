import * as d3 from 'd3';
import * as moment from moment;

const dennyd=[];

d3.csv('../dataSet/denny.csv', (data) =>{
    for(let i=0; i< data.length;i++){
        let startTime = `${data[i].startTime} ${data[i].startap}`;
        let endTime = `${data[i].stopTime} ${data[i].stopap}`;
        let duration = moment.duration(endTime.diff(startTime));
        let hours = duration.asHours();
        dennyd.push(hours);

    }
});

console.log(dennyd);
